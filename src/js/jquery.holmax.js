/*
*   Wavify
*   Jquery Plugin to make some nice waves
*   by peacepostman @ crezeo
 */
(function ( $ ) {

    $.fn.wavify = function( options ) {

        //  Options
        //
        //
        var settings = $.extend({
            container: options.container ? options.container : 'body',
            // Height of wave
            height: 200,
            // Amplitude of wave
            amplitude: 100,
            // Animation speed
            speed: .15,
            // Total number of articulation in wave
            bones: 3,
            // Color
            color: 'rgba(255,255,255, 0.20)'
        }, options );

        var wave = this,
            width = $(settings.container).width(),
            height = $(settings.container).height(),
            points = [],
            lastUpdate,
            totalTime = 0;

        //  Set color
        //
        TweenLite.set(wave, {attr:{fill: settings.color}});


        function drawPoints(factor) {
            var points = [];

            for (var i = 0; i <= settings.bones; i++) {
                var x = i/settings.bones * width;
                var sinSeed = (factor + (i + i % settings.bones)) * settings.speed * 2000;
                var sinHeight = Math.sin(sinSeed / 100) * settings.amplitude;
                var yPos = Math.sin(sinSeed / 100) * sinHeight  + settings.height;
                points.push({x: x, y: yPos});
            }

            return points;
        }

        function drawPath(points) {
            var SVGString = 'M ' + points[0].x + ' ' + points[0].y;

            var cp0 = {
                x: (points[1].x - points[0].x) / 2,
                y: (points[1].y - points[0].y) + points[0].y + (points[1].y - points[0].y)
            };

            SVGString += ' C ' + cp0.x + ' ' + cp0.y + ' ' + cp0.x + ' ' + cp0.y + ' ' + points[1].x + ' ' + points[1].y;

            var prevCp = cp0;
            var inverted = -1;

            for (var i = 1; i < points.length-1; i++) {
                var cpLength = Math.sqrt(prevCp.x * prevCp.x + prevCp.y * prevCp.y);
                var cp1 = {
                    x: (points[i].x - prevCp.x) + points[i].x,
                    y: (points[i].y - prevCp.y) + points[i].y
                };

                SVGString += ' C ' + cp1.x + ' ' + cp1.y + ' ' + cp1.x + ' ' + cp1.y + ' ' + points[i+1].x + ' ' + points[i+1].y;
                prevCp = cp1;
                inverted = -inverted;
            }

            SVGString += ' L ' + width + ' ' + height;
            SVGString += ' L 0 ' + height + ' Z';
            return SVGString;
        }

        //  Draw function
        //
        //
        function draw() {
            var now = window.Date.now();

            if (lastUpdate) {
                var elapsed = (now-lastUpdate) / 3000;
                lastUpdate = now;

                totalTime += elapsed;

                var factor = totalTime*Math.PI;
                TweenMax.to(wave, settings.speed, {
                    attr:{
                        d: drawPath(drawPoints(factor))
                    },
                    ease: Power1.easeInOut
                });

            } else {
                lastUpdate = now;
            }

            requestAnimationFrame(draw);
        }

        //  Pure js debounce function to optimize resize method
        //
        //
        function debounce(func, wait, immediate) {
            var timeout;
            return function() {
                var context = this, args = arguments;
                clearTimeout(timeout);
                timeout = setTimeout(function() {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                }, wait);
                if (immediate && !timeout) func.apply(context, args);
            };
        }

        //  Redraw for resize with debounce
        //
        var redraw = debounce(function() {
            wave.attr('d', '');
            points = [];
            totalTime = 0;
            width = $(settings.container).width();
            height = $(settings.container).height();
            lastUpdate = false;
            setTimeout(function(){
                draw();
            }, 50);
        }, 250);
        $(window).on('resize', redraw);


        //  Execute
        //
        return draw();

    };

}(jQuery));

$(function() {
		$('#headerVaweOne path').wavify({
		container: 'header',
	  height: 85,
	  bones: 5,
	  amplitude: 80,
	  speed: 0.016,
	  color: 'url(#PSgrad_1)'
	});

	$('#headerVaweTwo path').wavify({
		container: 'header',
	  height: 99,
	  bones: 4,
	  amplitude: 88,
	  speed: 0.014,
	  color: 'url(#PSgrad_2)'
	});
	$('#headerVaweThree path').wavify({
		container: 'header',
	  height: 88,
	  bones: 5,
	  amplitude: 84,
	  speed: 0.012,
	  color: 'url(#PSgrad_3)'
	});
	$('#featVaweOne path').wavify({
			container: '.feat',
		  height: 60,
		  bones: 5,
		  amplitude: 57,
		  speed: .014,
		  color: 'url(#PSgrad_0)',
		  color: 'url(#cardsVawesGrad01)'
		});
	$('#featVaweTwo path').wavify({
			container: '.feat',
		  height: 110,
		  bones: 4,
		  amplitude: 60,
		  speed: .017,
		  color: 'url(#PSgrad_1)',
		  color: 'url(#cardsVawesGrad11)'
		});
		
	$('#formsVaweOne path').wavify({
		container: '.forms',
	  height: 120,
	  bones: 6,
	  amplitude: 58,
	  speed: .015,
	  color: 'url(#formsVaweOneGrad)'
	});

	$('#formsVaweTwo path').wavify({
		container: '.forms',
	  height: 110,
	  bones: 5,
	  amplitude: 58,
	  speed: .017,
	  color: 'url(#formsVaweTwoGrad)'
	});
	$('#formsVaweThree path').wavify({
		container: '.forms',
	  height: 100,
	  bones: 5,
	  amplitude: 56,
	  speed: .014,
	  color: 'url(#formsVaweThreeGrad)'
	});

	$('.js-toForm').on('click', function(e) {
		e.preventDefault();
		var target = $('.form-block').offset().top;

		$('html, body').animate({
				scrollTop: target
		}, 2000, 'swing');

	});
	$('.box__laptop').on('click', function(e) {
		e.preventDefault();
		var target = $('.feat h2').offset().top;

		$('html, body').animate({
				scrollTop: target
		}, 2000, 'swing');

	});

	var $video = $('.feat-video').find('video');
	$video.removeAttr('controls');
	$('.video__trigger').on('click', function() {
		$(this)
			.hide()
			.prev()
			.get(0)
			.play();
		$video.attr('controls', '');
	});

	$('.footer__soc').find('a').on('mouseover', function() {
		$(this).siblings().css('opacity', '0.5');
	});
	$('.footer__soc').find('a').on('mouseout', function() {
		$(this).siblings().css('opacity', '1');
	});

	setTimeout(function(){	
		$('.header__logo').find('svg g:first-child').addClass('scaleOut');
	}, 1000);
	
});


