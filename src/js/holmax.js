
var body = document.getElementsByTagName('body');

toggleClassHeader()

window.onscroll = function() {
  toggleClassHeader()
}

function toggleClassHeader( ){
	var header = document.querySelector('.header--top');
	var scrolled = window.pageYOffset || document.documentElement.scrollTop;
  var scrolledActive = 10;

  if(scrolled > scrolledActive){
  	header.classList.add("header--top-scroll");
  } else {
  	header.classList.remove("header--top-scroll");
  }
}



// Таймер
function startTimer(duration, display_min, display_sec) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display_min.textContent = minutes;
        display_sec.textContent = seconds;

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

window.onload = function () {
    var tenMinutes = 60 * 10,
        min = document.querySelector('.supeline--value-min'),
        sec = document.querySelector('.supeline--value-sec');
    startTimer(tenMinutes, min, sec);
};

   


